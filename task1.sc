/*author: keppnew@gmail.com*/

// #1

def task1_decrypt() = {
  var i = 12
  while (i < 20) {
    var cipherText = "GHMABGZ VKXTMXL LNVVXLL EBDX GHG-LMHI, XGMANLBTLMBV XYYHKM"

    val alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    println("-----------------------------")
    val shift = (alphabet.size - i) % alphabet.size
    val outputText = cipherText.map((c: Char) => {
      val x = alphabet.indexOf(c.toUpper)
      if (x == -1) {
        c
      }
      else {
        alphabet((x + shift) % alphabet.size)
      }
    })
    println(outputText)
    i = i + 1
  }
}
task1_decrypt()

println(s"#1 - NOTHING CREATES SUCCESS LIKE NON-STOP, ENTHUSIASTIC EFFORT")

// #2

val str2 = "10001011101010101010000111110111011110101010101101110101010101010010000010110100101010101011011010100101011010101010101010101010101110101011000101101011110101010101010101010001010101010101101010101010101010101010101010111000001010101111010100111010101001011101010111111111101010101111111101010111110101001010101111110111101011010111111101011110101111111111111101111111111010101111101010101001111101010101010100100101010111101001010101001010101001010111110101010101010101011110101010010101001111101010100101111101010101001111111111101010111111111101001010111111110110101001111101010101111111010110100011111111111010101101011111110101010101110101010101010001110111101010101010101010101000001010110111111010101010010101011110101010000001010101000000000000101001111100000000000010010101010000001"
val str1 = "11100101000010101000001010010000010101011000110000110101000001010100000010000000010101100000110100100010111111111111111010010001010000001000000100000101011110101000000001010100000001010100101010111001010100000000000010101010101101010010101010101111001010000000000000001010010100111000010000000010100001010101000000110000001010101000000000000101001111100000000000010010101010000001"

def task2(str1: String, str2: String): Int = {
  val arr1 = str1.toCharArray.map((x: Char)=>{
    if (x=='1') 1
    else 0
  }).reverse
  val arr2 = str2.toCharArray.map((x: Char)=>{
    if (x=='1') 1
    else 0
  }).reverse
  var q = 0
  val res = arr1.zipAll(arr2, 0, 0).map { case (a, b) => {
    a+b+q match{
      case 0 => q=0
        0
      case 1 => q=0
        1
      case 2 => q=1
        0
      case 3 => q=1
        1
    }
  }}
  res.filter(_>0).length-res.filter(_<1).length
}

println(s"#2 - ${task2(str1, str2)}")

// #3

def task3(n: Int): Int = {
  def isPalindrome(x:Int) = {
    val s = x.toBinaryString
    s == s.reverse
  }
  Stream.from(1).filter(x => isPalindrome(x)).take(n).sum
}
println(s"#3 - ${task3(73)}")

// #4

val arr = Array(-1, -1, -2, -2, 1, -5, 1, 0, 1, 14, -8, 4, 5, -11, 13, 5, 7, -10, -4, 3, -6, 8, 6, 2, -9, -1, -4, 0)
def task4(arr: Array[Int]): Int ={
  arr.combinations(3).filter(_.sum == 0).length
}

println(s"#4 - ${task4(arr)}")

// #5

val filepath = "C:\\Users\\ds\\Downloads\\recfun\\src\\main\\scala\\recfun\\task5.txt"
def task5(filepath: String): BigInt = {
  val sumOfNums = scala.io.Source.fromFile(filepath)
    .getLines.toList.map(BigInt(_)).sum
  sumOfNums/BigInt("10").pow(sumOfNums.toString(10).length-10)
}
println(s"#5 - ${task5(filepath)}")




